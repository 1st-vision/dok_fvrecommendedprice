.. |label| replace:: 1st Vision UVP
.. |snippet| replace:: FvRecommendedPrice
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.2.0
.. |maxVersion| replace:: 5.4
.. |version| replace:: 1.0.4
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Durch dieses Plugin wird eine unverbindliche Preisempfehlung unter dem Artikelpreis im Frontend angezeigt. Die Preisliste für den UVP kann im Backend ausgewählt werden.

Frontend
--------
Wenn der Preis der hinterlegten UVP-Kundengruppe größer als der Verkaufspreis ist, wird dieser mit dem Text:
"UVP: " angezeigt.

.. image:: FvRecommendedPrice1.png



Backend
-------

Grundeinstellungen
__________________
.. image:: FvRecommendedPrice2.png

:Brutto/Netto UVP anhand des eingeloggten Kunden: Wenn UVP nicht nach eingeloggten Kunden berechnet werden soll, dann wird UVP immer in Brutto angezeigt
:UVP Kundengruppe: Die Kundengruppe aus der die Preise für UVP genommen werden, soll angegeben werden



Textbausteine
_____________

:frontend/fv_recommended_price/detail/data:


technische Beschreibung
------------------------
keine


Modifizierte Template-Dateien
-----------------------------
:/detail/data.tpl:





